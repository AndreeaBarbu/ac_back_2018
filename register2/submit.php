<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$sex = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$faculty = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";


if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['sex'])){
	$sex = $_POST['sex'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['faculty'])){
	$faculty = $_POST['faculty'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}


if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($cnp) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($faculty) || empty($captcha_generated) || empty($captcha_inserted) || empty($check)){
	$error = 1;
	$error_text = "One or more fields are empty!";
}

if(strlen($firstname) < 3 || strlen($firstname) > 20 || strlen($lastname) < 3 || strlen($lastname) > 20){
	$error = 1;
	$error_text = "First or last name is not valid";
}

if(strlen($question) < 15){
	$error = 1;
	$error_text = "Answer shorter than expected!";
}

if(is_numeric($firstname) || is_numeric($lastname)){
	$error = 1;
	$error_text = "Names cannot contain numbers!";
}

if(strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid";
}

if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
	$error = 1;
	$error_text = "Email is not valid";
}




if(strlen($cnp)!=13){
	$error = 1;
	$error_text = "CNP is not valid";
}


if(!filter_var($facebook, FILTER_VALIDATE_URL)){
	$error = 1;
	$error_text = "Facebook link is not valid";
}

if(is_numeric($faculty) || strlen($faculty) < 3 || strlen($faculty) > 30){
	$error = 1;
	$error_text = "Faculty is not valid";
}

if($cnp{0}>6){
	$error = 1;
	$error_text = "CNP is not valid";
}

if($cnp{0}==0){
	$error = 1;
	$error_text = "CNP is not valid";
}

if($cnp{0}==1 || $cnp{0}==3 || $cnp{0}==5){
	$sex = "M";
} else {
	$sex = "F";
}

if($captcha_inserted!=$captcha_generated){
	$error = 1;
	$error_text = "The inserted code is not correct";
}

function age(&$birth)
{

	$date = new DateTime($birth);
	$now = new DateTime();
	$interval = $now->diff($date);
	return $interval->y;
}


if(age($birth)<18 || age($birth)>100){
	$error = 1;
	echo "Inappropriate age";
}






function wpf_dev_form_entry_total( $atts ) {

	$args = shortcode_atts( array(
		'form_id' => ''
	), $atts );

	if(empty($atts['form_id'])){
		return;
	}

	$total = wpforms()->entry->get_entries( array('form_id' => $atts['form_id']), true);

	if($total==50){
	$error = 1;
	}







try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}

$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes";
}


